# Inatallation

[install h2 database](http://www.h2database.com/html/download.html) and run it:
java -cp h2{version}.jar org.h2.tools.Server -ifNotExists


# Usage

Save contact with github repositories and search with multiple feilds

# Input

Save Api :
PUT /contacts
input format :
```
{
    "body" :{
        "name" :"",
        "phoneNumber" : <Long> ,
        "email" : "",
        "organization" : "",
        "githubName" : ""
    }
}
```


Search Api : 
POST /contacts/search
input format :
```
{
    "body" :{
        "name" :"",
        "phoneNumber" : <Long>,
        "email" : "",
        "organization" : "",
        "githubName" : ""
    }
}
```
