package com.asredanesh.contacts.controller;

import com.asredanesh.contacts.dto.BaseDto;
import com.asredanesh.contacts.dto.Response;
import com.asredanesh.contacts.dto.UserDto;
import com.asredanesh.contacts.exeptions.AlreadyExistsException;
import com.asredanesh.contacts.exeptions.NotFoundException;
import com.asredanesh.contacts.exeptions.NullPointerException;
import com.asredanesh.contacts.service.UserService;
import com.asredanesh.contacts.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("contacts")
public class UserController {

    @Autowired
    UserService userService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping(value = "" , produces = {"application/json; charset=UTF-8"})
    public Response<UserDto> add(@RequestBody BaseDto<UserDto> dto ){
        try{
            return new Response<>(userService.save(dto.getBody()), Constant.Code.CREATE);
        }catch (AlreadyExistsException e){
            return new Response<>(Constant.Code.ALREADY_EXIST, e.getMessage());
        }catch (NullPointerException e){
            return new Response<>(Constant.Code.NOT_ALLOW, e.getMessage());
        }
    }

    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @PostMapping(value = "/search" , produces = {"application/json; charset=UTF-8"})
    public Response<List<UserDto>> search(@RequestBody BaseDto<UserDto> dto){
        try{
            return new Response<>(userService.search(dto.getBody()));
        }catch (NotFoundException e){
            return new Response<>(Constant.Code.NOT_FOUND, e.getMessage());
        }
    }
}
