package com.asredanesh.contacts.dao;

import com.asredanesh.contacts.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User , Long>, QueryByExampleExecutor<User> {

    @Query(value = "SELECT true FROM tbl_user WHERE c_phone =:phone" , nativeQuery = true)
    Boolean isExist(@Param("phone") long phone);

}
