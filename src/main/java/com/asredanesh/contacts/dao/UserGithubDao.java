package com.asredanesh.contacts.dao;

import com.asredanesh.contacts.entity.UserGithub;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGithubDao extends CrudRepository<UserGithub , Long> {
}
