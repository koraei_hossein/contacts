package com.asredanesh.contacts.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.gson.annotations.SerializedName;

public class BaseDto<T> {

    @JsonProperty("body")
    private T body;

    public BaseDto(T body) {
        this.body = body;
    }

    public BaseDto() {
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "BaseDto{" +
                "body=" + body +
                '}';
    }
}
