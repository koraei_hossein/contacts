package com.asredanesh.contacts.dto;

public class GithubDto {

    private long id;
    private String repositoryLink;

    public GithubDto(long id, String repositoryLink) {
        this.id = id;
        this.repositoryLink = repositoryLink;
    }
}
