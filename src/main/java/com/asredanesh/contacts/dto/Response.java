package com.asredanesh.contacts.dto;

import com.asredanesh.contacts.utils.Constant;

public class Response<T> {

    private T response;
    private Integer response_code;
    private String message;

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public Integer getResponseCode() {
        return response_code;
    }

    public void setResponseCode(Integer responseCode) {
        this.response_code = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response(T response) {
        this.response = response;
        response_code = Constant.Code.OK;
    }

    public Response(Integer responseCode) {
        this.response_code = responseCode;
    }

    public Response(T response, Integer responseCode) {
        this.response = response;
        this.response_code = responseCode;
    }

    public Response(Integer response_code, String message) {
        this.response_code = response_code;
        this.message = message;
    }
}
