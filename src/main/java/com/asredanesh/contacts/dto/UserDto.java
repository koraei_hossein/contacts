package com.asredanesh.contacts.dto;

import com.asredanesh.contacts.entity.User;

import java.util.List;

public class UserDto {

    private String name;
    private Long phoneNumber;
    private String email;
    private String organization;
    private String githubName;

    public UserDto(String name, Long phone, String email, String organization, String githubName) {
        this.name = name;
        this.phoneNumber = phone;
        this.email = email;
        this.organization = organization;
        this.githubName = githubName;
    }

    public static UserDto create(User user){
        return new UserDto(user.getName(), user.getPhoneNumber(), user.getEmail(), user.getOrganization(), user.getGithubLink());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGithubName() {
        return githubName;
    }

    public void setGithubName(String githubLink) {
        this.githubName = githubLink;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                " name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", organization='" + organization + '\'' +
                ", githubLink='" + githubName + '\'' +
                '}';
    }
}
