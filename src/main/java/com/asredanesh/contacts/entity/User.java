package com.asredanesh.contacts.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tbl_user")
public class User {

    @Id
    @Column(name = "c_phone")
    private Long phoneNumber;

    @Column(name = "c_name")
    private String name;

    @Column(name = "c_email")
    private String email;

    @Column(name = "c_organization")
    private String organization;

    @Column(name = "c_github")
    private String githubName;

    @OneToMany(fetch = FetchType.LAZY , mappedBy = "user" , cascade = CascadeType.ALL)
    private Set<UserGithub> userGithubRepositories = new HashSet<>();

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGithubName() {
        return githubName;
    }

    public void setGithubName(String githubName) {
        this.githubName = githubName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGithubLink() {
        return githubName;
    }

    public void setGithubLink(String githubLink) {
        this.githubName = githubLink;
    }

    public Set<UserGithub> getUserGithubRepositories() {
        return userGithubRepositories;
    }

    public void setUserGithubRepositories(Set<UserGithub> userGithubRepositories) {
        this.userGithubRepositories = userGithubRepositories;
    }

    @Override
    public String toString() {
        return "User{" +
                "phone=" + phoneNumber +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", organization='" + organization + '\'' +
                ", githubName='" + githubName + '\'' +
                ", userGithubRepositories=" + userGithubRepositories +
                '}';
    }
}
