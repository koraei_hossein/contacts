package com.asredanesh.contacts.entity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_user_github")
public class UserGithub {

    @Id
    @Column(name = "c_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "c_repository")
    private String repositoryLink;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "c_user_id" , nullable = false)
    private User user;

    public UserGithub() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRepositoryLink() {
        return repositoryLink;
    }

    public void setRepositoryLink(String repositoryLink) {
        this.repositoryLink = repositoryLink;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
