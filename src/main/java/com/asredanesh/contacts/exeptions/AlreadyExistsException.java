package com.asredanesh.contacts.exeptions;

public class AlreadyExistsException extends Exception {

    public AlreadyExistsException(String message) {
        super(String.format("%s already exists." , message));
    }
}
