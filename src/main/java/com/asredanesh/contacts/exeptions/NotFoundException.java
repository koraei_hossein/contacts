package com.asredanesh.contacts.exeptions;

public class NotFoundException extends Exception {
    public NotFoundException(String message) {
        super(String.format("%s not found" , message));
    }

    public NotFoundException() {
    }
}
