package com.asredanesh.contacts.exeptions;

public class NullPointerException extends Exception {

    public NullPointerException(String message) {
        super(String.format("%s can not be empty" , message));
    }
}
