package com.asredanesh.contacts.httpUtils;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class NetworkManager {


    public static String getResponseFromUrl(String url){

        String result;
        if(url == null)
            return null;

        try {
            URL url1 = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(url1.openStream()));
            result = in.readLine();
            in.close();
            return result;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
}
