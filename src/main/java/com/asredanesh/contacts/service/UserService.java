package com.asredanesh.contacts.service;

import com.asredanesh.contacts.dao.UserDao;
import com.asredanesh.contacts.dao.UserGithubDao;
import com.asredanesh.contacts.dto.UserDto;
import com.asredanesh.contacts.entity.User;
import com.asredanesh.contacts.entity.UserGithub;
import com.asredanesh.contacts.exeptions.AlreadyExistsException;
import com.asredanesh.contacts.exeptions.NotFoundException;
import com.asredanesh.contacts.exeptions.NullPointerException;
import com.asredanesh.contacts.httpUtils.NetworkManager;
import com.asredanesh.contacts.httpUtils.NetworkUtils;
import com.asredanesh.contacts.pojo.model.GithubRepository;
import com.asredanesh.contacts.utils.Converter;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserDao dao;

    @Autowired
    UserGithubDao userGithubDao;

    public UserService() {
    }

    public UserDto save(UserDto userDto) throws AlreadyExistsException, NullPointerException {

        if(userDto.getPhoneNumber() == null)
            throw new NullPointerException("phone");

        if (isExist(userDto.getPhoneNumber()))
            throw new AlreadyExistsException(String.valueOf(userDto.getPhoneNumber()));

        dao.save(getUserFromDto(userDto));

        if (userDto.getGithubName() != null)
            getGithubRepositoryFromUrl(userDto.getPhoneNumber(), userDto.getGithubName());

        return userDto;
    }

    private void getGithubRepositoryFromUrl(long phone, String githubName) {

        GithubRepository[] githubRepositories = new Gson().fromJson(NetworkManager
                        .getResponseFromUrl(NetworkUtils.GITHUB_BASE_REPOSITORY_URL
                                + githubName + NetworkUtils.GITHUB_END_REPOSITORY_URL)
                , GithubRepository[].class);

        for (GithubRepository githubRepository : githubRepositories) {
            saveUserGithubRepository(phone, githubRepository.getHtmlUrl());
        }
    }

    private void saveUserGithubRepository(long phone, String repoLink) {

        UserGithub userGithub = new UserGithub();
        userGithub.setUser(dao.findById(phone).get());
        userGithub.setRepositoryLink(repoLink);
        userGithubDao.save(userGithub);

    }

    public List<UserDto> search(UserDto userDto) throws NotFoundException {

        ExampleMatcher userExampleMatcher = ExampleMatcher.matchingAll()
                .withIgnoreNullValues()
                .withNullHandler(ExampleMatcher.NullHandler.IGNORE);
        Iterable<User> users = dao.findAll(Example.of(getUserFromDto(userDto), userExampleMatcher));
        if (Converter.getListFromIterator(users).isEmpty())
            throw new NotFoundException(String.valueOf(userDto.getPhoneNumber()));
        return Converter.getListFromIterator(users).stream().map(UserDto::create).collect(Collectors.toList());
    }

    private User getUserFromDto(UserDto userDto) {

        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setOrganization(userDto.getOrganization());
        user.setGithubLink(userDto.getGithubName());

        return user;
    }

    boolean isExist(long phone) {
        return dao.isExist(phone) != null;
    }
}
