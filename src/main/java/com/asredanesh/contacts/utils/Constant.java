package com.asredanesh.contacts.utils;

public class Constant {

    public static class Code {
        public static final int OK = 700;
        public static final int CREATE = 701;
        public static final int ACCEPT = 702;
        public static final int NOT_ALLOW = 703;
        public static final int NOT_FOUND = 704;
        public static final int ALREADY_EXIST = 705;
        public static final int UPDATE = 706;
        public static final int DELETE = 707;
    }
}
