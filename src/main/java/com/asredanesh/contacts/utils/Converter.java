package com.asredanesh.contacts.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Converter {

    public static <T> List<T>
    getListFromIterator(Iterable<T> iterator)
    {
        List<T> list = new ArrayList<>();
        iterator.forEach(list::add);
        return list;
    }
}
